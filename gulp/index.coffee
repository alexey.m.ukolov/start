gulp       = require 'gulp'
sequence   = require 'gulp-sequence'
requireDir = require 'require-dir'

module.exports = () ->
	tasks = requireDir './tasks', recurse: true
	gulp.task name, task for name, task of tasks

	gulp.task 'default', (cb) ->
		global.ENV = 'prod'
		sequence 'default:dev', cb

	gulp.task 'default:dev', (cb) ->
		sequence 'clean', ['jade', 'compass', 'browserify', 'img', 'fonts'], cb

	gulp.task 'dev', (cb) ->
		global.ENV = 'dev'
		sequence 'default:dev', ['webServer', 'jsonServer', 'watch'], cb
