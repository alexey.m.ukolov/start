gulp   = require 'gulp'
watch  = require 'gulp-watch'
config = require 'config'
tasks  = config.watch

module.exports = () ->
	for task in tasks
		src = config[task].watchSrc || config[task].src
		gulp.watch src, [task]