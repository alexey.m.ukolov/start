gulp       = require 'gulp'
gulpif     = require 'gulp-if'
uglify     = require 'gulp-uglify'
browserify = require 'browserify'
source     = require 'vinyl-source-stream'
buffer     = require 'vinyl-buffer'
glob       = require 'glob'
path       = require 'path'
es         = require 'event-stream'

onError    = require 'onError'
config     = require('config').browserify

module.exports = () ->
	files  = glob.sync config.src

	streams = files.map (file) ->
		config.options['entries'] = file
		config.options['debug'] = if ENV == 'prod' then false else true

		return browserify config.options
			.bundle()
			.on 'error', (err) ->
				onError.bind(@) err
			.pipe source path.basename(file, path.extname file) + '.js'
			.pipe buffer()
			.pipe gulpif ENV == 'prod', uglify()
			.pipe gulp.dest config.dest

	return es.merge streams