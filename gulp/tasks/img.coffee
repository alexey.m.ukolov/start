gulp     = require 'gulp'
gulpif   = require 'gulp-if'
imagemin = require 'gulp-imagemin'

onError = require 'onError'
config  = require('config').img

module.exports = () ->
	gulp.src config.src
		.pipe gulpif ENV == 'prod', imagemin optimizationLevel: config.optimizationLevel
		.pipe gulp.dest config.dest