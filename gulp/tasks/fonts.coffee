gulp   = require 'gulp'

config = require('config').fonts

module.exports = () ->
	gulp.src config.src
		.pipe gulp.dest config.dest