$ = require 'jquery'
window.jQuery = $

Foundation = require './foundation'

# plugins
select2 = require 'select2'

# my module
mock = require('./modules/mockjax')()

$ ->
	$('select').select2
		theme: 'primary'

	return