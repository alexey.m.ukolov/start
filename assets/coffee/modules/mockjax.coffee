$ = require 'jquery'
mockjax = require('jquery-mockjax')($)

mock = () ->
	$.mockjax
		url: 'action.php'
		responseTime: 1000
		response: ->
			response =
				status: 'success'
				message: 'Данные успешно сохранены.'

			@responseText = JSON.stringify response

module.exports = mock