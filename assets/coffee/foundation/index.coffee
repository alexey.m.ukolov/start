require 'foundation-sites/js/foundation.core'

Foundation =
	Abide: require 'foundation-sites/js/foundation.abide'
	Reveal: require 'foundation-sites/js/foundation.reveal'
	# more modules...

require 'foundation-sites/js/foundation.util.box'
require 'foundation-sites/js/foundation.util.keyboard'
require 'foundation-sites/js/foundation.util.mediaQuery'
require 'foundation-sites/js/foundation.util.motion'
require 'foundation-sites/js/foundation.util.nest'
require 'foundation-sites/js/foundation.util.timerAndImageLoader'
require 'foundation-sites/js/foundation.util.touch'
require 'foundation-sites/js/foundation.util.triggers'

module.exports = Foundation